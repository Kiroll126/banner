<?php
class Emagedev_Banners_Model_Widget_Instance extends Mage_Widget_Model_Widget_Instance
{
    protected function _beforeSave()
    {
    	$parametres = $this->getData('widget_parameters');
    	if($parametres['title']){
    		$title = 'widget_key_'.$parametres['title'];
    		Mage::app()->GetCacheInstance()->clean(array($title));
    	}
        return parent::_beforeSave();
    }
}