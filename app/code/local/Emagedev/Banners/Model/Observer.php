<?php
class Emagedev_Banners_Model_Observer 
{
	public function widgetSaveOnPage($observer){
		//Mage::app()->cleanCache(array('widget_banner'));
	}

	public function widgetInstanceSave($observer){
		Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('emagedevbanners')->__('Banner successfully deleted'));  
		Mage::app()->cleanCache(array('widget_instance_banner'));
		$widget = $observer->getEvent()->getObject();
    	$parameters = $widget->getWidgetParameters();
		if($parameters['title']){
    		$title = 'widget_key_'.$parameters['title'];
    		Mage::app()->cleanCache(array($title));
    	}
	}

}