<?php
class Emagedev_Banners_Model_Banner extends Mage_Core_Model_Abstract
{
	protected $imagePath = 'emagedevbanners';

    protected function _construct()
    {
        $this->_init('emagedevbanners/banner');
    }


    protected function _beforeSave()
    {
        return parent::_beforeSave();
    }
    
    public function getImagePath()
    {
        return Mage::getBaseDir('media') . DS . $this->imagePath . DS;
    }
    
    public function setImage($image)
    {
        if ($image instanceof Varien_File_Uploader) {
            $image->save($this->getImagePath());
            $image = $image->getUploadedFileName();
        }
        $this->setData('image', $image);
        return $this;
    }
    
    public function getImage()
    {
        if ($image = $this->getData('image')) {
            return Mage::getBaseUrl('media') . $this->imagePath . DS . $image;
        } else {
            return false;
        }
    }

    public function getImageName()
    {
        if ($image = $this->getData('image')) {
            return $image;
        } else {
            return false;
        }
    }
}