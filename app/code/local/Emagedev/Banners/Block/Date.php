<?php

class Emagedev_Banners_Block_Date extends Mage_Adminhtml_Block_Widget_Grid
{
   
   public function __construct($arguments=array())
    {
        parent::__construct($arguments);
    } 

    public function prepareElementHtml(Varien_Data_Form_Element_Abstract $element)
    {  
        $date_field = new Varien_Data_Form_Element_Date(
            array(
                'name' => 'date',
                'after_element_html' => '<small>Click icon to select</small>',
                'tabindex' => 1,
                'type'  => 'datetime',
                'time'=>true,
                'image' => $this->getSkinUrl('images/grid-cal.gif'),
                'format' => 'yyyy-MM-dd HH:mm:ss',
            )
        );    

        $date_field->setForm($element->getForm());
        $id = 'date'.$element->getHtmlId();
        $date_field->setId($id);
        $html = $date_field->getElementHtml();    
        $html .= "
            <style>
                .calendar{z-index:999999;margin-left:20%;}
                #$id{display:none}
            </style>
            <script>
                if(!input_date_start) var input_date_start =  document.getElementById('".$element->getHtmlId()."');    
                else var input_date_end =  document.getElementById('".$element->getHtmlId()."');
                if(!input_varien_start){
                    var input_varien_start =  document.getElementById('".$id."');
                    input_varien_start.onchange = function() {
                        input_date_start.value = input_varien_start.value;
                    }; 
                }else{
                    var input_varien_end =  document.getElementById('".$id."');
                    input_varien_end.onchange = function() {
                        input_date_end.value = input_varien_end.value;
                    }; 
                }          
            </script>";    
        $element->setData('after_element_html', $html);
        return $element;
    }
}
