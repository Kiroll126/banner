<?php
class Emagedev_Banners_Block_Adminhtml_Banners extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    protected function _construct()
    {
        $this->_blockGroup = 'emagedevbanners';
        $this->_controller = 'adminhtml_banners';
        $this->_headerText = $this->__('Banners');
        $this->_addButtonLabel = $this->__('Add New Banner');
    }
}
