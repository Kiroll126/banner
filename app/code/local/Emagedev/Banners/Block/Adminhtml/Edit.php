<?php
class Emagedev_Banners_Block_Adminhtml_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    protected function _construct()
    {
        $this->_blockGroup = 'emagedevbanners';
        $this->_mode = 'edit';
        $this->_controller = 'adminhtml';
        
        $banner_id = (int)$this->getRequest()->getParam($this->_objectId);
        if(!$banner_id) {
        
        }
        $banner = Mage::getModel('emagedevbanners/banner')->load($banner_id);
        Mage::register('current_banner', $banner);
        $this->_removeButton('reset');
    }

     public function getHeaderText()
    {
        $banner = Mage::registry('current_banner');
        if ($banner->getId()) { 
            return $this->__("Edit Banner");
        } else {
            return $this->__("Add new Banner");
        }
    }
}
