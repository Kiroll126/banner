<?php
class Emagedev_Banners_Block_Adminhtml_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
    {
        $banner = Mage::registry('current_banner');
        $form = new Varien_Data_Form(array(
            'enctype'=> 'multipart/form-data'
        ));
        $fieldset = $form->addFieldset('edit_banner', array(
            'legend' => $this->__('Banner Details')
        ));

        if ($banner->getId()) {
            $fieldset->addField('id', 'hidden', array(
                'name'      => 'id',
                'required'  => true
            ));
        }
 
        $fieldset->addField('title', 'text', array(
            'name'      => 'title',
            'title'     => $this->__('title'),
            'label'     => $this->__('Title'),
            'maxlength' => '250',
            'required'  => true,
        ));

        $fieldset->addField('type_link', 'select', array(
            'name'      => 'type_link',
            'values'   =>  array(array('label'=>$this->__('Inside link'),'value'=>'inside'),array('label'=>$this->__('Outside link'),'value'=>'outside')), 
            'title'     => $this->__('type_link'),
            'label'     => $this->__('Type of link'),
            'maxlength' => '250',
            'required'  => true,
        ));

        $fieldset->addField('link', 'text', array(
            'name'      => 'link',
            'title'     => $this->__('link'),
            'label'     => $this->__('Link'),
            'maxlength' => '250',
            'required'  => true,
        ));
        
        $fieldset->addField('date_start', 'date', array(
            'name'      => 'date_start',
            'title'     => $this->__('date_start'),
            'label'     => $this->__('Date start'),
            'image'  => $this->getSkinUrl('images/grid-cal.gif'),
            'input_format' => 'yyyy-MM-dd HH:mm:ss',
            'format'       => 'yyyy-MM-dd HH:mm:ss',
            'time' => true,
            'required'  => true,
        ));

        $fieldset->addField('date_end', 'date', array(
            'name'      => 'date_end',
            'title'     => $this->__('date_end'),
            'label'     => $this->__('Date end'),
            'image'  => $this->getSkinUrl('images/grid-cal.gif'),
            'input_format' => 'yyyy-MM-dd HH:mm:ss',
            'format'       => 'yyyy-MM-dd HH:mm:ss',
            'time' => true,
            'required'  => true,
        ));

        $fieldset->addField('is_active', 'select', array(
            'name'      => 'is_active',
            'values'    =>  array(array('label'=>$this->__('Yes'),'value'=>'1'),array('label'=>$this->__('No'),'value'=>'0')),
            'title'     => $this->__('is_active'),
            'label'     => $this->__('Is active'),
        ));

        $fieldset->addField('image', 'image', array(
            'name'      => 'image',
            'label'     => $this->__('Image'),
            'required'  => true,
        ));
 
 		$form->setMethod('post');
        $form->setUseContainer(true);
        $form->setId('edit_form');
        $form->setAction($this->getUrl('*/*/save'));
        $data = $banner->getData();
        $data['image'] = $banner->getImage();
        $form->setValues($data);
        $this->setForm($form);
    }
}
