<?php
class Emagedev_Banners_Block_Adminhtml_Banners_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
 
    protected function _construct()
    {
        $this->setId('bannersGrid');
        $this->setUseAjax(true);
        
        $this->setDefaultSort('id');
        $this->setDefaultDir('desc');
    }
 
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('emagedevbanners/banner')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
 
    protected function _prepareColumns()
    { 
        $this->addColumn('id', array(
            'header'        => $this->__('ID'),
            'align'         => 'right',
            'width'         => '20px',
            'filter_index'  => 'id',
            'index'         => 'id'
        ));

        $this->addColumn('image', array(
            'header'        => $this->__('Image'),
            'align'         => 'center',
            'filter_index'  => 'image',
            'index'         => 'image',
            'width'     => '250px',
            'renderer' => 'Emagedev_Banners_Block_Adminhtml_Renderer_Image',
            'filter'    => false,
            'sortable'  => false,
        ));
 
        $this->addColumn('title', array(
            'header'        => $this->__('Title'),
            'align'         => 'center',
            'filter_index'  => 'title',
            'index'         => 'title',
            'type'          => 'text',
            'truncate'      => 50,
            'escape'        => true,
        ));

        $this->addColumn('date_start', array(
            'header'        => $this->__('Date start'),
            'align'         => 'center',
            'filter_index'  => 'date_start',
            'index'         => 'date_start',
            'type'          => 'datetime',
            'truncate'      => 50,
            'escape'        => true,
        ));

        $this->addColumn('date_end', array(
            'header'        => $this->__('Date end'),
            'align'         => 'center',
            'filter_index'  => 'date_end',
            'index'         => 'date_end',
            'type'          => 'datetime',
            'truncate'      => 50,
            'escape'        => true,
        ));

        

        $this->addColumn('link', array(
            'header'        => $this->__('Link'),
            'align'         => 'center',
            'filter_index'  => 'link',
            'index'         => 'link',
            'type'          => 'text',
            'truncate'      => 50,
            'escape'        => true,
        ));

        $this->addColumn('is_active', array(
            'header'        => $this->__('Is active'),
            'align'         => 'center',
            'filter_index'  => 'is_active',
            'index'         => 'is_active',
            'type' => 'options',
            'options'=>  array('1'=>$this->__('Yes'),'0'=>$this->__('No')),
            'renderer' => 'Emagedev_Banners_Block_Adminhtml_Renderer_Active',
            'width'     => '20px',
            'sortable'  => false,
        ));


        $this->addColumn('action', array(
            'header'    => $this->__('Action'),
            'width'     => '50px',
            'type'      => 'action',
            'getter'     => 'getId',
            'actions'   => array(
                array(
                    'caption' => $this->__('Edit'),
                    'url'     => array(
                        'base'=>'*/*/edit',
                    ),
                    'field'   => 'id'
                    ),
                ),
            'filter'    => false,
            'sortable'  => false,
            'index'     => 'id',
        ));
 
        return parent::_prepareColumns();
    }

    public function getRowUrl($banners)
    {
        return $this->getUrl('*/*/edit', array(
            'id' => $banners->getId(),
        ));
    }
    
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
}
