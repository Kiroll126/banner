<?php
class Emagedev_Banners_Block_Adminhtml_Banners_Chooser extends Mage_Adminhtml_Block_Widget_Grid
{
  /**
     * Block constructor, prepare grid params
     *
     * @param array $arguments
     */
    public function __construct($arguments=array())
    {
        parent::__construct($arguments);
        $this->setUseAjax(true);
        $this->setDefaultSort('id');
        $this->setDefaultDir('desc');
    }

    /**
     * Prepare chooser element HTML
     *
     * @param Varien_Data_Form_Element_Abstract $element Form Element
     * @return Varien_Data_Form_Element_Abstract
     */
    public function prepareElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $uniqId = Mage::helper('core')->uniqHash($element->getId());
        $sourceUrl = $this->getUrl('*/banners/chooser', array('uniq_id' => $uniqId));

        $chooser = $this->getLayout()->createBlock('emagedevbanners/adminhtml_block')
            ->setElement($element)
            ->setTranslationHelper($this->getTranslationHelper())
            ->setConfig($this->getConfig())
            ->setFieldsetId($this->getFieldsetId())
            ->setSourceUrl($sourceUrl)
            ->setUniqId($uniqId);

        if ($element->getValue()) {
            $array_banner_id = preg_split("/[\s,]+/", $element->getValue());
            $nambers = array();
            for($i=0,$j=0;$i<count($array_banner_id);$i=$i+3,$j++){
                $numbers[$j][0] = $array_banner_id[$i+2];
                $numbers[$j][1] = $array_banner_id[$i+1];
                $numbers[$j][2] = $array_banner_id[$i];
            }
            for($i = 0; $i<count($numbers)-1;$i++){
                $divId = $numbers[$i][2]." ".$numbers[$i][1]." ";
                $url = Mage::getModel('emagedevbanners/banner')->load($numbers[$i][1])->getImage();
                $images[$i] =   "<div class='banners_grid' id='".$divId."'><p>"
                                .$this->__('Banner').":</p><img src='". $url ."'style='max-width:100px;max-height:100px;'/>".
                                "<p>".$this->__('Priority').": </p><input value='".$numbers[$i][0]."' onchange =\"var val = this.parentElement.id;".
                                "var str = ".$uniqId.".getElementValue();".
                                "var index_start = str.indexOf(val);".
                                "var index_end = str.indexOf(',',index_start);".
                                "var res = str.substring(0,index_start)+val+this.value+str.substring(index_end,str.length);"
                                .$uniqId.".setElementValue(res);\"></input>".
                                "<button onclick=\"var val = this.parentElement.id;".
                                                    "var str = ".$uniqId.".getElementValue();".
                                                    "var label = ".$uniqId.".getElementLabel();".
                                                    "var index_start = str.indexOf(val);".
                                                    "var div_id = '".$divId."';".
                                                    "var div = document.getElementById(div_id);".
                                                    "div.parentNode.removeChild(div);".
                                                    "var index_end = str.indexOf(',',index_start)+1;".
                                                    "var res = str.substring(0,index_start)+str.substring(index_end,str.length);"
                                                    .$uniqId.".setElementValue(res);"
                                                    .$uniqId.".setElementLabel(label.innerHTML);\" type=\"button\">"
                                                    .$this->__('delete')."</button></div>";
            } 
            $chooser->setLabel(implode(' ',$images));
        }

        $html = $chooser->toHtmlWithTag();
        $element->setData('after_element_html', $html);
        return $element;
    }

    public function getRowClickCallback()
    {
        $chooserJsObject = $this->getId();
        $js = '
            function (grid, event) {
                var trElement = Event.findElement(event, "tr");
                var blockId = trElement.down("td").innerHTML.replace(/^\s+|\s+$/g,"");
                var count = '.$chooserJsObject.'.getElementLabel().getElementsByTagName("div").length;
                var divId = count +" "+blockId+" ";
                var blockTitle = "<div class=\'banners_grid\' id=\""+divId+"\"><p>'
                                .$this->__('Banner').':</p> "+trElement.down("td").next().innerHTML+"<p>'
                                .$this->__('Priority').': </p><input value=\"1\""+
                                "onchange = \"var val = this.parentElement.id; var str = '.$chooserJsObject.'.getElementValue();"+
                                "var index_start = str.indexOf(val);"+
                                "var index_end = str.indexOf(\',\',index_start);"+
                                "var res = str.substring(0,index_start)+val+this.value+str.substring(index_end,str.length);"+
                                "'.$chooserJsObject.'.setElementValue(res);\"></input>"+
                                "<button onclick=\"var val = this.parentElement.id;"+
                                                    "var str = '.$chooserJsObject.'.getElementValue();"+
                                                    "var label = '.$chooserJsObject.'.getElementLabel();"+
                                                    "var index_start = str.indexOf(val);"+
                                                    "var div = document.getElementById(val);"+
                                                    "div.parentNode.removeChild(div);"+
                                                    "var index_end = str.indexOf(\',\',index_start)+1;"+
                                                    "var res = str.substring(0,index_start)+str.substring(index_end,str.length);"+
                                                    "'.$chooserJsObject.'.setElementValue(res);"+
                                                    "'.$chooserJsObject.'.setElementLabel(label.innerHTML);\" type=\"button\">'
                                                    .$this->__('delete').'</button></div>";
                if('.$chooserJsObject.'.getElementValue()){
                    '.$chooserJsObject.'.setElementValue('.$chooserJsObject.'.getElementValue()+count+" "+blockId+" 1,");
                    '.$chooserJsObject.'.setElementLabel('.$chooserJsObject.'.getElementLabel().innerHTML+" "+blockTitle);
                }else{
                    '.$chooserJsObject.'.setElementValue(count+" "+blockId+" 1,");
                    '.$chooserJsObject.'.setElementLabel(blockTitle);
                }
                '.$chooserJsObject.'.close();
            }
        ';
        return $js;
    }
    /**
     * Prepare rules collection
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('emagedevbanners/banner')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Prepare columns for rules grid
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
      
        $this->addColumn('id', array(
            'header'        => $this->__('ID'),
            'align'         => 'right',
            'width'         => '20px',
            'filter_index'  => 'id',
            'index'         => 'id'
        ));

        $this->addColumn('image', array(
            'header'        => $this->__('Image'),
            'align'         => 'center',
            'filter_index'  => 'image',
            'index'         => 'image',
            'width'     => '250px',
            'renderer' => 'Emagedev_Banners_Block_Adminhtml_Renderer_Image',
            'filter'    => false,
            'sortable'  => false,
        ));
 
        $this->addColumn('title', array(
            'header'        => $this->__('Title'),
            'align'         => 'center',
            'filter_index'  => 'title',
            'index'         => 'title',
            'type'          => 'text',
            'truncate'      => 50,
            'escape'        => true,
        ));

        $this->addColumn('link', array(
            'header'        => $this->__('Link'),
            'align'         => 'center',
            'filter_index'  => 'link',
            'index'         => 'link',
            'type'          => 'text',
            'truncate'      => 50,
            'escape'        => true,
        ));

        $this->addColumn('is_active', array(
            'header'        => $this->__('Is active'),
            'align'         => 'center',
            'filter_index'  => 'is_active',
            'index'         => 'is_active',
            'type' => 'options',
            'options'=>  array('1'=>$this->__('Yes'),'0'=>$this->__('No')),
            'renderer' => 'Emagedev_Banners_Block_Adminhtml_Renderer_Active',
            'width'     => '20px',
            'sortable'  => false,
        ));

        return parent::_prepareColumns();
    }

    /**
     * Prepare grid URL
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/banners/chooser', array('_current' => true));
    }
}
?>