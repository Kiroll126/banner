<?php
class Emagedev_Banners_Block_Widget extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface 
{
    protected function _construct()
    {
        parent::_construct();
        $tags = array();
        if($this->getTitle()){
            $array_banner_id = preg_split("/[\s,]+/", $this->getBannerId());
            $nambers = array();
            for($i=0,$j=0;$i<(count($array_banner_id)-1);$i=$i+3,$j++){
                $numbers[$j] = $array_banner_id[$i+1];
            }
           
            for($i=0;$i<count($numbers);$i++){
                $tags[$i] = Mage_Catalog_Model_Product::CACHE_TAG.'_widget_ban_'.$numbers[$i];
            }
            $tags[count($numbers)] = 'widget_key_'.$this->getTitle();
            $tags[(count($numbers)+1)] = 'widget_banner';
        }else{
            $tags[0] = 'widget_instance_banner';
        }
        $this->addData(array(
            'cache_lifetime'    => Mage_Core_Model_Cache::DEFAULT_LIFETIME,
            'cache_tags'        => $tags,
            'cache_key'         => 'widget_key_'.$this->getTitle().$this->getDateStart(),
        ));
        $date = Mage::getModel('core/date')->date('Y-m-d H:i:s');
        $date_end = $this->getDateEnd();
        if(!(($date>$date_start)&&($date<$date_end))){
            Mage::app()->GetCacheInstance()->clean(array('widget_key_'.$this->getTitle()));
        }
        for($i=0;$i<count($numbers);$i++){
            if($date > $this->getBanner($numbers[$i])['date_end']){
                Mage::app()->GetCacheInstance()->clean(array(Mage_Catalog_Model_Product::CACHE_TAG.'_widget_ban_'.$numbers[$i]));
            }
        }
    }

    protected function getBanner($id)
    {
        $banner = array();
        $block = Mage::getModel('emagedevbanners/banner')->load($id);
        if($block->getTypeLink()=='outside'){
            $banner['link'] = 'http://'.$block->getLink();
        }else{
            $banner['link'] = $block->getLink();
        } 
        $banner['path'] = $block->getImage();
        $banner['is_active'] = $block->getIsActive();
        $banner['date_end'] = $block->getDateEnd();
        $banner['date_start'] = $block->getDateStart();
        return $banner;
    }

    protected function getWidget(){
        $widget = array();
        $widget['interval'] = $this->getInterval()*1000;
        $date_start = $this->getDateStart();
        $date_end = $this->getDateEnd();
        $date = Mage::getModel('core/date')->date('Y-m-d H:i:s');
        $widget['count'] = 0;
        $array_banner_id = preg_split("/[\s,]+/", $this->getBannerId());
        $nambers = array();
        for($i=0,$j=0;$i<count($array_banner_id);$i=$i+3,$j++){
        	$numbers[$j][0] = $array_banner_id[$i+2];
        	$numbers[$j][1] = $array_banner_id[$i+1];
        	$numbers[$j][2] = $array_banner_id[$i];
        }
        array_multisort($numbers);
        for($i=0;$i<count($numbers);$i++){
        	$numbers[$i] = $numbers[$i][1];
        }
        if(($date>$date_start)&&($date<$date_end)){
            for($i=0;$i<count($numbers);$i++){
                if($numbers[$i]&&$this->getBanner($numbers[$i])['is_active']){
                    if(($date < $this->getBanner($numbers[$i])['date_end'])&&($date>$this->getBanner($numbers[$i])['date_start'])){
                        $widget['banners'][$widget['count']] = $this->getBanner($numbers[$i]);
                        $widget['count']++;
                    }
                }
            }
        }
        return $widget; 
    }

    protected function _toHtml()
    {     
        return parent::_toHtml();
    }
}