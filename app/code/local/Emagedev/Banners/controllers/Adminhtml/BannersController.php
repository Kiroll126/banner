<?php
class Emagedev_Banners_Adminhtml_BannersController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->_title($this->__('Banners'));
        $this->loadLayout();
       	$this->_setActiveMenu('emagedevbanners');
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_title($this->__('Add new banner'));
        $this->loadLayout();
        $this->_setActiveMenu('emagedevbanners');
        $this->renderLayout();
    }

     public function deleteAction()
    {
        $tipId = $this->getRequest()->getParam('id', false);
 
        try {
            Mage::getModel('emagedevbanners/banner')->setId($tipId)->delete();
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('emagedevbanners')->__('Banner successfully deleted'));           
            return $this->_redirect('*/*/');
        } catch (Mage_Core_Exception $e){
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        } catch (Exception $e) {
            Mage::logException($e);
            Mage::getSingleton('adminhtml/session')->addError($this->__('Somethings went wrong'));
        }
 
        $this->_redirectReferer();
    }

    public function saveAction()
    {
        $data = $this->getRequest()->getPost();
        
        $tag = Mage_Catalog_Model_Product::CACHE_TAG.'_widget_ban_'.$data['id'];
        Mage::app()->GetCacheInstance()->clean(array($tag));
        
        if (!empty($data)) {
            try {
                try{
                    $uploader = new Varien_File_Uploader('image');
                    $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
                    $uploader->setAllowRenameFiles(true);    
                }
                catch (Exception $e){
                    if(Mage::getModel('emagedevbanners/banner')->setData($data)->getImage()){
                        $image = Mage::getModel('emagedevbanners/banner')->load($data['id'])->getImageName();
                        Mage::getModel('emagedevbanners/banner')->setData($data)->setImage($image)->save();
                        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('emagedevbanners')->__('Banner successfully saved'));
                        return $this->_redirect('*/*/');
                    }else{
                        Mage::getSingleton('adminhtml/session')->addError($this->__('Images not upload'));
                        return $this->_redirectReferer();
                    }
                }     
                Mage::getModel('emagedevbanners/banner')->setData($data)->setImage($uploader)->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('emagedevbanners')->__('Banner successfully saved'));
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->addError($this->__('Somethings went wrong'));
            }
        }
        return $this->_redirect('*/*/');
    }

    public function editAction()
    {
        $this->_title($this->__('Edit banner'));
        $this->loadLayout();
        $this->_setActiveMenu('emagedevbanners');
        $this->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('emagedevbanners/adminhtml_banners_grid')->toHtml()
        );
    }

    public function chooserAction()
    {
        $uniqId = $this->getRequest()->getParam('uniq_id');
        $chooserBlock = $this->getLayout()->createBlock('emagedevbanners/adminhtml_banners_chooser', '', array(
            'id' => $uniqId
        ));
        $this->getResponse()->setBody($chooserBlock->toHtml());
    }
}
