<?php
$installer = $this;
$installer->startSetup();
 
$installer->run("
CREATE TABLE `emagedev_banners` (
 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
 `title` varchar(255),
 `link` varchar(255),
 `type_link` varchar(255),
 `image` varchar(255),
 `date_start` datetime,
 `date_end` datetime,
 `is_active` boolean,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
");
 
$installer->endSetup();