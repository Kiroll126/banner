function Widget(count,banners,banners_id,widgets_id,interval){
		
	this.count = count;
	this.banners = banners;
	this.banners_id = banners_id;
	this.widget_id = widget_id;	
	this.interval = interval;
	
	var widget = document.getElementById(this.banners_id);
		
	for(var i = 0; i<this.banners.length; i++){
		widget.innerHTML+= '<div class="swiper-slide"><a href="'+this.banners[i]['link']+'"><img src="'+this.banners[i]['path']+'"></a></div>';
	}	

	var swiper = new Swiper('#'+this.widget_id, {
        pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        paginationClickable: true,
        spaceBetween: 30,
        centeredSlides: true,
        autoplayDisableOnInteraction: false, 
        effect: 'fade',
        loop: true
    });
    swiper.params.autoplay = this.interval;
    swiper.startAutoplay();
}

window.onload=function(){

};
